/* eslint-disable */
module.exports = {
  "extends": "@snowpack/app-scripts-react",
  "scripts": {
    "run:lint": "eslint 'src/**/*.{js,jsx,ts,tsx}'",
    "run:lint::watch": "watch \"$1\" src"
  },
  "plugins": [
    "@snowpack/plugin-dotenv",
    "@snowpack/plugin-webpack"
  ],
  installOptions: {
    rollup: {
      plugins: [require("rollup-plugin-node-polyfills")()]
    }
  }
}
