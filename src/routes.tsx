import React, { ReactElement } from 'react';
import { useSelector } from 'react-redux';
import { BrowserRouter, Redirect, Route, RouteProps } from 'react-router-dom';
import Authentication from './features/authen';
import Home from './features/home';
import type { Reducers } from './store';


interface RouteEnchant extends RouteProps {
  ensureLogged?: boolean
}

const routes: RouteEnchant[] = [
  {
    path: '/',
    ensureLogged: true,
    component: Home,
  },
  {
    path: '/login',
    component: Authentication,
  },
];

function PrivateRoute(props: RouteEnchant) {
  const { userInfo } = useSelector((state: Reducers) => state.auth);
  // eslint-disable-next-line react/prop-types
  const { component: Component, path } = props;

  return (
    <Route
      path={path}
      render={props => {
        if (userInfo) {
          // eslint-disable-next-line @typescript-eslint/ban-ts-comment
          // @ts-ignore
          return <Component {...props} />;
        }
        return <Redirect
          to={{
            pathname: '/login',
            state: { from: path },
          }}
        />;
      }}
    />
  );
}

function Routes(): ReactElement {
  return (
    <BrowserRouter>
        {
          routes.map((route: RouteEnchant) => {
            if (route.ensureLogged) {
              return (
                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                // @ts-ignore
                <PrivateRoute {...route} TargetComponent={route.component} key={route.path}/>
              );
            }
            return (
              // eslint-disable-next-line @typescript-eslint/ban-ts-comment
              // @ts-ignore
              <Route {...route} key={route.path}/>
            );
          })
        }
    </BrowserRouter>
  );
}


export default Routes;

