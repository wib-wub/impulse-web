import React, { ReactElement } from 'react';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { useStyle } from 'styled-hooks';
import { cancelRequestLogin, signOut } from '../features/authen/store';

interface Props {
  children: ReactElement[] | ReactElement
}

function BasicLayout(props: Props): ReactElement {

  const dispatch = useDispatch();

  const appStyle = useStyle`
    min-height: 100vh;
    margin: 0;
  `;

  const appContainerStyle = useStyle`
    margin-top: 15px;
  `;

  const handleLogout = () => {
    dispatch(signOut())
    dispatch(cancelRequestLogin());
  };

  return (
    <div className={appStyle}>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container-fluid">
          <NavLink to="/" className="navbar-brand">Impulse</NavLink>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                  aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"/>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto mb-2 mb-lg-0">
              <li className="nav-item">
                <NavLink className="nav-link" to="/">Home</NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link disabled" to="/profile">Profile</NavLink>
              </li>
            </ul>
            <div className="d-flex">
              <button className="btn btn-secondary" onClick={handleLogout}>Logout</button>
            </div>
          </div>
        </div>
      </nav>
      <div className={`container ${appContainerStyle}`}>
      {props.children}
      </div>
    </div>
  );
}

export default BasicLayout;
