import React from 'react';
import { useStyle } from 'styled-hooks';

interface Props {
  children: React.ReactElement
}

function LoginLayout(props: Props): React.ReactElement {
  const main = useStyle`
    min-height: 100vh;
    text-align: center;
  `;
  return (
    <div className={main}>
      {props.children}
    </div>
  );
}

export default LoginLayout;
