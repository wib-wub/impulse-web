export const firebaseConfig = {
  apiKey: import.meta.env.SNOWPACK_PUBLIC_FIREBASE_API_KEY,
  authDomain: import.meta.env.SNOWPACK_PUBLIC_FIREBASE_AUTH_DOMAIN,
  databaseURL: import.meta.env.SNOWPACK_PUBLIC_FIREBASE_DATABASE_URL,
  projectId: import.meta.env.SNOWPACK_PUBLIC_FIREBASE_PROJECT_ID,
  storageBucket: import.meta.env.SNOWPACK_PUBLIC_FIREBASE_STRORAGE_BUCKET,
  messagingSenderId: import.meta.env.SNOWPACK_PUBLIC_FIREBASE_MESSAGING_SENDER_ID,
  appId: import.meta.env.SNOWPACK_PUBLIC_FIREBASE_APPID,
  measurementId: import.meta.env.SNOWPACK_PUBLIC_FIREBASE_MESSUREMENT_ID,
};

export const facebook = {
  url: import.meta.env.SNOWPACK_PUBLIC_FB_URL,
  appId: import.meta.env.SNOWPACK_PUBLIC_FB_APP_ID,
  appSecret: import.meta.env.SNOWPACK_PUBLIC_FB_APP_SECRET,
};
