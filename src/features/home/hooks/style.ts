import { useStyle } from 'styled-hooks';

export interface UseHomeStyle {
  clickable: string
}

export const useHomeStyle = (): UseHomeStyle => {

  const clickable = useStyle`
    cursor: pointer;
  `

  return {
    clickable
  }
}
