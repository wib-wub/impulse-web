import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { firebaseAuth } from '../../../services/firebase';
import type { Reducers } from '../../../store';
import type { Page } from '../../authen/store';
import type { Comment, LiveVideo, Order } from '../store';
import {
  cancelRequestComment,
  cancelRequestPost,
  requestComment,
  requestPost,
  setSelectPage,
  setSelectPost,
  cancelRequestOrders,
  requestOrders, requestProducts, cancelRequestProducts,
} from '../store/actions';

interface Hook {
  orders: Order[],
  liveVideos: LiveVideo[],
  comments: Comment[],
  pages: Page[],
  selectedPost: LiveVideo | null
  selectedPage: Page | null
  handleSelectPage: (page: Page) => void,
  handleSelectPost: (post: LiveVideo) => void
  clearFocus: () => void,
}

function usePostManager(): Hook {
  const dispatch = useDispatch();
  const { liveVideos, selectedPage, comments, selectedPost, orders } = useSelector((state: Reducers) => state.home);
  const { pages } = useSelector((state: Reducers) => state.auth);

  const handleSelectPage = (page: Page): void => {
    dispatch(cancelRequestOrders());
    dispatch(cancelRequestComment());
    dispatch(cancelRequestPost());
    dispatch(setSelectPost(null))
    dispatch(requestPost(page.id, page.accessToken));
    dispatch(setSelectPage(page));
  };

  const handleSelectPost = (post: LiveVideo): void => {
    dispatch(cancelRequestOrders());
    dispatch(cancelRequestComment());
    dispatch(requestComment(post, selectedPage?.accessToken as string));
    dispatch(setSelectPost(post));
    dispatch(requestOrders(post.id));
  };

  const clearFocus = (): void => {
    dispatch(cancelRequestOrders());
    dispatch(cancelRequestComment());
    dispatch(setSelectPost(null))
  }

  useEffect(() => {
    dispatch(requestProducts(firebaseAuth.currentUser?.uid as string))
    return () => {
      dispatch(cancelRequestComment());
      dispatch(cancelRequestPost());
      dispatch(cancelRequestOrders())
      dispatch(cancelRequestProducts())
      dispatch(setSelectPost(null));
      dispatch(setSelectPage(null));
    };
  }, []);

  return {
    liveVideos,
    comments,
    selectedPost,
    pages,
    orders,
    selectedPage,
    handleSelectPage,
    handleSelectPost,
    clearFocus,
  };
}

export default usePostManager;
