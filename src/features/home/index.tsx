import React from 'react';
import BasicLayout from '../../layouts/basic.layout';
import { CommendCard, LiveSelectCard, PageSelectCard } from './components';
import OrderCard from './components/Order-Card';
import usePostManager from './hooks/PostManager';

function Home(): React.ReactElement {
  const { pages, selectedPage, liveVideos, comments, selectedPost, handleSelectPage, handleSelectPost, orders, clearFocus } = usePostManager();


  return (
    <BasicLayout>
      <div className="row row-cols-1 g-2">
        <div className="col collapse focusPostMode show" id="page_collapse">
          <PageSelectCard pages={pages} handleSelectPage={handleSelectPage} selectedPage={selectedPage}/>
        </div>
        <div className="col">
          <div className="row row-cols-1 g-2">
            <div className="collapse focusPostMode m-1 mb-2">
        <button className="btn btn-secondary" type="button" data-toggle="collapse" data-target=".focusPostMode"
                aria-expanded="false" aria-controls="page_collapse post_collapse order_collapse comment_collapse" onClick={clearFocus}>Back</button>
      </div>
        <div className="col focusPostMode show" id="post_collapse">
          <LiveSelectCard
            liveVideos={liveVideos}
            selectedPage={selectedPage}
            handleSelectPost={handleSelectPost}
            selectPost={selectedPost}
          />
        </div>
        <div className="col">
          <div className="row row-cols-2 g-2">
            <div className="col focusPostMode" id="order_collapse">
              <OrderCard Orders={orders} Post={selectedPost}/>
            </div>
            <div className="col focusPostMode" id="comment_collapse">
              <CommendCard comments={comments} selectPost={selectedPost}/>
            </div>
          </div>
        </div>
          </div>
        </div>
      </div>
    </BasicLayout>
  );
}

export default Home;
