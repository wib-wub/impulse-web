import type { Page } from '../../authen/store';
import type { LiveVideo, Product, RequestPost } from './types';
import { Comment, Order, types } from './types';

export const setLivePost = (livePost: LiveVideo[]): Action<LiveVideo[]> => ({
  type: types.setLive,
  payload: livePost,
});

export const setComment = (comments: Comment[]): Action<Comment[]> => ({
  type: types.setComments,
  payload: comments,
});

export const pushComment = (comment: Comment): Action<Comment> => ({
  type: types.pushComment,
  payload: comment,
});

export const setSelectPage = (page: Page | null): Action<Page | null> => ({
  type: types.setSelectPage,
  payload: page,
});

export const requestPost = (pageId: string, accessToken: string): Action<RequestPost> => ({
  type: types.requestPost,
  payload: {
    pageId,
    accessToken,
  },
});

export const cancelRequestPost = (): Action<void> => ({
  type: types.cancelRequestPost,
});

export const setSelectPost = (post: LiveVideo | null): Action<LiveVideo | null> => ({
  type: types.setSelectPost,
  payload: post,
});

export const requestComment = (post: LiveVideo, accessToken: string): Action<{ post: LiveVideo, accessToken: string }> => ({
  type: types.requestComment,
  payload: {
    post,
    accessToken,
  },
});

export const cancelRequestComment = (): Action<void> => ({
  type: types.cancelRequestComment,
});

export const setOrders = (orders: Order[]): Action<Order[]> => ({
  type: types.setOrders,
  payload: orders,
});

export const requestOrders = (postId: string): Action<string> => ({
  type: types.requestOrders,
  payload: postId,
});

export const cancelRequestOrders = (): Action<void> => ({
  type: types.cancelRequestOrder,
});

export const setProducts = (products: Product[]): Action<Product[]> => ({
  type: types.setProducts,
  payload: products,
});

export const requestProducts = (uid: string): Action<string> => ({
  type: types.requestProducts,
  payload: uid,
});

export const cancelRequestProducts = (): Action<void> => ({
  type: types.cancelRequestProducts,
});


