import type { Page } from '../../authen/store';
import type { HomeReducer, LiveVideo, Order, Product } from './types';
import { types, Comment } from './types';

const initState: HomeReducer = {
  comments: [],
  liveVideos: [],
  selectedPage: null,
  selectedPost: null,
  orders: [],
  products: [],
};

function homeReducer(state = initState, action: Action<LiveVideo | Comment[] | Page | LiveVideo[] | null | Comment | Order[] | Product[]>): HomeReducer {
  switch (action.type) {
    case types.setLive:
      return <HomeReducer>{ ...state, liveVideos: action.payload };
    case types.setComments:
      return <HomeReducer>{ ...state, comments: action.payload };
    case types.setSelectPage:
      return <HomeReducer>{ ...state, selectedPage: action.payload };
    case types.setSelectPost:
      return <HomeReducer>{ ...state, selectedPost: action.payload };
    case types.pushComment:
      return <HomeReducer>{ ...state, comments: [...state.comments, action.payload] }
    case types.setOrders:
      return <HomeReducer>{ ...state, orders: action.payload }
    case types.setProducts:
      return <HomeReducer>{ ...state, products: action.payload }
    default:
      return state;
  }
}

export default homeReducer;
