import type { Page } from '../../authen/store';

export enum VideoStatus {
  live = 'LIVE',
  prelive = 'PRE_LIVE',
  vod = 'VOD'
}

export enum OrderStatus {
  waitingForPay = 'wait_pay',
  paid = 'paid',
}

export interface Customer {
  id: string;
  fbId: string;
  name: string;
}

export interface LiveVideo {
  id: string;
  status: VideoStatus;
  livesView: number;
  createdAt: Date;
}

export interface Comment {
  id: string;
  content: string;
  from: {
    id: string;
    photo: string;
    name: string;
  };
  createdAt: Date;
  paginate?: {
    next: string;
    prev: string;
  }
}

export interface Product {
  id: string;
  name: string;
  keywords: string[];
  createdAt: Date;
  images: string[];
  amount?: number;
  inStock: number;
}

export interface Order {
  id: string;
  customerId: string;
  owner: string;
  postId: string;
  products: Product[];
  status: OrderStatus;
  createdAt: Date;
}

export interface HomeReducer {
  liveVideos: LiveVideo[];
  comments: Comment[];
  selectedPage: Page | null;
  selectedPost: LiveVideo | null;
  orders: Order[];
  products: Product[];
}

export enum types {
  setLive = 'SET_LIVE',
  setComments = 'SET_COMMENT',
  requestComment = 'REQUEST_COMMENT',
  cancelRequestComment = 'CANCEL_REQUEST_COMMENT',
  requestPost = 'REQUEST_POST',
  cancelRequestPost = 'CANCEL_REQUEST_POST',
  setSelectPage = 'SET_SELECT_PAGE',
  setSelectPost = 'SET_SELECT_POST',
  pushComment = 'PUSH_COMMENT',
  requestOrders = 'REQUEST_ORDERS',
  cancelRequestOrder = 'CANCEL_REQUEST_ORDERS',
  setOrders = 'SET_ORDERS',
  requestProducts = 'REQUEST_PRODUCTS',
  cancelRequestProducts = 'CANCEL_REQUEST_PRODUCTS',
  setProducts = 'SET_PRODUCTS'
}

export interface RequestPost {
  accessToken: string;
  pageId: string;
}

export interface RequestComment {
  post: LiveVideo;
  accessToken: Page['accessToken'];
  paginate: string;
}
