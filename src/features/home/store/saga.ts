import axios from 'axios';
import type { END as EndType, EventChannel } from 'redux-saga';
import { END, eventChannel } from 'redux-saga';
import type { CallEffect, PutEffect, RaceEffect, TakeEffect } from 'redux-saga/effects';
import { all, call, cancel, cancelled, fork, put, race, take } from 'redux-saga/effects';
import { facebook } from '../../../config';
import { firebaseFirestore } from '../../../services/firebase';
import { cancelRequestComment, pushComment, setComment, setLivePost, setOrders, setProducts } from './actions';
import { Comment, LiveVideo, Order, Product, RequestComment, types, VideoStatus } from './types';

async function getProductsByUid(uid) {
  const docs = await firebaseFirestore.collection('Products').where('owner', '==', uid).get()
  const products: Product[] = []
  docs.forEach(doc => {
    products.push({
      createdAt: doc.data().createAt,
      inStock: doc.data().in_stock as number,
      id: doc.id,
      images: doc.data().images,
      keywords: doc.data().keywords,
      name: doc.data().name,
    })
  })
  return products
}

function* getProductFlow(uid: string) {
  try {
    const products =  yield call(getProductsByUid, uid)
    yield put(setProducts(products))
  } catch (e) {
    console.error(e)
  } finally {
    if(yield cancelled()) {
      yield put(setProducts([]))
    }
  }
}

function* getProductWatcher() {
  while(true) {
    const { payload } = yield take(types.requestProducts)
    const task = yield fork(getProductFlow, payload)
    const action = yield take(types.cancelRequestProducts)
    if(action.type === types.cancelRequestProducts) {
      yield cancel(task)
    }
  }
}

function getOrders(postId: string) {
  return eventChannel(emitter => {
    const ref = firebaseFirestore.collection('Orders')
      .where('post_id', '==', postId)
      ref.onSnapshot({ includeMetadataChanges: true},(snapshot) => {
        const orders: Order[] = []
        snapshot.forEach(doc => {
          orders.push({
              ...doc.data() as Order,
              id: doc.id,
            customerId: doc.data()?.customer_id,
            createdAt:  doc.data()?.createdAt?.toDate()
          })
        })

        emitter(orders)
      })
    return () => ref
  })
}

function* orderReceiver(req) {
  const channel = yield call(getOrders, req)

  try {
    while (true) {
      const orders = yield take(channel)
      yield put(setOrders(orders))
    }
  } catch (e) {
    console.error(e)
    channel.close()
  } finally {
    if(yield cancelled()) {
      channel.close()
    }
  }
}

function getLiveCommentEventSource(eventSource: EventSource): EventChannel<Action<Comment> | EndType | Action<void>> {
  return eventChannel((emitter) => {
    eventSource.onmessage = (msg) => {
      const data = JSON.parse(msg.data);
      return emitter(pushComment({
        from: {
          name: data.from.name,
          id: data.from.id,
          photo: `http://graph.facebook.com/${data.from.id}/picture?type=square`,
        },
        content: data.message,
        id: data.id,
        createdAt: new Date(),
      }));
    };
    eventSource.onerror = () => {
      return emitter(END);
    };
    eventSource.addEventListener('disconnect', () => emitter(cancelRequestComment()), false);

    return () => {
      return eventSource.close();
    };
  });
}

async function getComments(post: LiveVideo, accessToken: string, paginateToken?: string): Promise<Comment[]> {
  const { data: res } = await axios.get(`${facebook.url}/${post.id}/comments?access_token=${accessToken}&fields=id,message,created_time,from&after=${paginateToken ?? ''}`);
  return res.data.map(comment => ({
    id: comment.id,
    content: comment.message,
    from: {
      id: comment.from.id,
      photo: `http://graph.facebook.com/${comment.from.id}/picture?type=square`,
      name: comment.from.name,
    },
    createdAt: new Date(comment.created_time),
    paginate: {
      next: res.paging.cursors.after,
      prev: res.paging.cursors.before,
    },
  }));
}

function* orderWatcher() {
  while (true) {
    const { payload } = yield take(types.requestOrders)
    const task = yield fork(orderReceiver, payload)
    const action = yield take(types.cancelRequestOrder)
    if (action.type === types.cancelRequestOrder) {
      yield cancel(task)
      yield put(setOrders([]))
    }
  }

}

function* receiveComments(req: RequestComment): Generator<EventSource | CallEffect | TakeEffect | PutEffect> {
  try {
    if (req.post.status === VideoStatus.live) {
      const result = new EventSource(`https://streaming-graph.facebook.com/${req.post.id}/live_comments?access_token=${req.accessToken}&comment_rate=one_per_two_seconds&fields=from{name,id},message`);
      const channel = yield call(getLiveCommentEventSource, result);
      while (true) {
        const action = yield take(channel as string);
        yield put(action as Action<Comment | void>);
      }
    } else {
      const comments = yield call(getComments, req.post, req.accessToken, req.paginate);
      yield put(setComment(comments as Comment[]));
      yield take(types.cancelRequestComment);
    }

  } catch (error) {
    console.error(error);
    yield put({ type: types.cancelRequestComment });
  }
}

function* liveCommentWatcher(): Generator<TakeEffect | RaceEffect<EventSource | CallEffect | TakeEffect | PutEffect> | PutEffect> {
  while (true) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const { payload } = yield take(types.requestComment);
    yield race([call(receiveComments, payload), take([types.cancelRequestComment])]);
    yield put(setComment([]));
  }
}

async function getPost(accessToken: string): Promise<LiveVideo> {
  const { data: request } = await axios.get(`${facebook.url}/me/live_videos?access_token=${accessToken}&fields=live_views,status,id,creation_time`);
  return request.data.map((post) => ({
    status: post.status,
    id: post.id,
    livesView: post.live_views,
    createdAt: new Date(post.creation_time),
  }));
}

function* getPostFlow(payload: { accessToken: string; }) {
  try {
    const posts = yield call(getPost, payload.accessToken);
    yield put(setLivePost(posts));
  } catch (e) {
    console.error(e);
  } finally {
    if (yield cancelled()) {
      yield put(setLivePost([]));
    }
  }
}

function* postWatcher() {
  while (true) {
    const { payload } = yield take(types.requestPost);
    const task = yield fork(getPostFlow, payload);
    const action = yield take([types.cancelRequestPost]);
    if (action.type === types.cancelRequestPost) {
      yield cancel(task);
    }
  }
}

export function* homeSaga(): Generator {
  yield all([
    liveCommentWatcher(),
    postWatcher(),
    orderWatcher(),
    getProductWatcher()
  ]);
}
