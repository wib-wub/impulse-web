import React, { ReactElement } from 'react';
import { useStyle } from 'styled-hooks';
import type { LiveVideo, Order } from '../store';


interface Props {
  Orders: Order[]
  Post: LiveVideo | null
}

function OrderCard(props: Props): ReactElement {

  const containerStyle = useStyle`
    max-height: 400px
  `
  return (
    <div className={`card ${containerStyle}`}>
      <div className="card-header">
        <span className="font-weight-bold">Orders from Post ID:</span> {props.Post?.id}
      </div>
      <div className="card-body overflow-auto">
        <div className="row row-cols-1 g-2">
        {
          props.Orders.map(order => (
            <div className="col" key={order.id}>
              <div className="card">
                <div className="card-header">
                  <div className="card-title">
                    Customer Id: {order.customerId}
                  </div>
                </div>
                <div className="card-body">
                  <table className="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                    {
                      order.products.map(item => (
                        <tr key={item.id}>
                          <td>{item.id}</td>
                          <td>{item.amount}</td>
                        </tr>
                      ))
                    }
                    </tbody>
                  </table>
                </div>
                <div className="card-footer">
                  {order.createdAt.toLocaleString()}
                </div>
              </div>
            </div>
          ))
        }
        </div>
      </div>
    </div>
  )
}

export default OrderCard
