import React, { ReactElement } from 'react';
import { useStyle } from "styled-hooks";
import type { Page } from '../../authen/store';
import { useHomeStyle } from '../hooks/style';

interface PropsTypes {
  pages: Page[];
  handleSelectPage: (page: Page) => void | null | undefined;
  selectedPage: Page | null;
}

function PageSelectCard(props: PropsTypes): ReactElement {
  const { clickable } = useHomeStyle()

  const containerStyle = useStyle`
    max-height: 300px;
  `
  const avatarStyle = useStyle`
    width: 60px;
    height: 60px;
  `

  return (
    <div className={`card ${containerStyle}`}>
      <div className="card-header">
        <span className="font-weight-bold">Pages</span>
      </div>
      <div className="card-body overflow-auto">
          <div className="row row-cols-1 row-cols-sm-1 row-cols-md-2 row-cols-lg-2 g-2">
          {
            props.pages.map((page) => (
              <div className="col" key={page.id} onClick={() => props?.handleSelectPage(page)}>
                <div className={`card w-100 h-100 ${ props.selectedPage?.id === page.id && 'border-secondary'} ${clickable}`}>
                  <div className="card-body">
                    <div className="row gx-1">
                      <div className="col">
                        <img className={`rounded-circle ${avatarStyle}`} src={page.picture} alt={`image-${page.id}`}/>
                      </div>
                      <div className="col">
                        <div>
                          {page.name}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            ))
          }
        </div>
        </div>
      </div>

  )
}

export default PageSelectCard
