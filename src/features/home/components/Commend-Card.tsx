import React, { ReactElement } from 'react';
import { useStyle } from 'styled-hooks';
import type { Comment, LiveVideo } from '../store';

interface PropTypes {
  comments: Comment[]
  selectPost: LiveVideo | null
}

function CommendCard(props: PropTypes): ReactElement {

  const avatarStyle = useStyle`
    width: 25px;
    height: 25px;
  `
  const containerStyle = useStyle`
    height: 300px;
  `
  return(
    <div className={`card ${containerStyle}`}>
      <div className="card-header">
        <span className="font-weight-bold">Comment from POST ID: {props.selectPost?.id}</span>
      </div>
      <div className="card-body text-center overflow-auto">
        <div className="row row-cols-1 g-1">
        {
          props.comments.length === 0 ?
            <div className="card-text">Please Select Your Live Post</div>
            :
            props.comments.map(comment => (
              <div className="col" key={comment.id}>
              <div  className="card">
                <div className="card-body">
                  <div className="row g-1">
                    <div className="col-1">
                      <img className={`rounded-circle ${avatarStyle}`} src={comment.from.photo} alt={`photo-${comment.from.id}`} />
                    </div>
                    <div className="col ml-2 text-left">
                      <div className="card-title">{comment.content} </div>
                      <div className="card-subtitle text-muted">
                        <small>{comment.from.name}</small> <small>{comment.createdAt.toLocaleString()}</small>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
                </div>)
            )
        }
        </div>
      </div>
    </div>

  )
}

export default CommendCard
