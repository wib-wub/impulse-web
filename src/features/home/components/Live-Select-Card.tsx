import React, { ReactElement, useEffect, useState } from 'react';
import type { Page } from '../../authen/store';
import { useHomeStyle } from '../hooks/style';
import type { LiveVideo } from '../store';
import { VideoStatus } from '../store';


interface LiveStatusBadgeProptypes {
  status: string;
}

function LiveStatusBadge(props: LiveStatusBadgeProptypes): ReactElement {

  const [color, setColor] = useState('bg-secondary');
  const [text, setText] = useState('End');

  useEffect(() => {
    switch (props.status) {
      case VideoStatus.live:
        setColor('bg-danger');
        setText('LIVE');
        break;
      case VideoStatus.vod:
        setColor('bg-success');
        setText('END');
        break;
      default:
        setColor('bg-secondary');
        setText('unknown');
    }
  }, [props.status]);


  return (
    <div className={`badge ${color}`}>
      {text}
    </div>
  );
}


interface Proptypes {
  liveVideos: LiveVideo[];
  selectedPage: Page | null;
  handleSelectPost: (post: LiveVideo) => void | null;
  selectPost: LiveVideo | null;
}

function LivePostSelectCard(props: Proptypes): ReactElement {
  const { clickable } = useHomeStyle();
  return (
    <div className="card">
        <div className="card-header">
          <span className="font-weight-bold">Live Post from :</span> {props.selectedPage?.name}
        </div>
        <div className="card-body">
          <div className="row">
            {
              props.liveVideos.map((live) => {
                return (
                  <div className={`col-12 ${clickable}`} key={live.id} onClick={() => props.handleSelectPost(live)}
                       data-toggle="collapse" data-target=".focusPostMode" aria-expanded="false"
                       aria-controls="page_collapse post_collapse order_collapse comment_collapse">
                  <div className={`card ${props.selectPost?.id === live.id && 'border-secondary'}`}>
                    <div className="card-body">
                      <div className="card-title">ID: {live.id} <LiveStatusBadge
                        status={live.status}/> Views: {live.livesView}</div>
                      <div className="card-text text-muted"><small>Created at {live.createdAt.toLocaleString()}</small></div>
                    </div>
                  </div>
                </div>
                );
              })
            }
          </div>
        </div>
      </div>
  );
}

export default LivePostSelectCard;
