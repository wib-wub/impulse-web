import { loginState, Page, types, UserInfo } from './types';


export const setUser = (data: UserInfo | null): Action<UserInfo | null> => ({
  type: types.setUser,
  payload: data,
});

export const setLoginState = (status: loginState): Action<loginState> => ({
  type: types.setLoginState,
  payload: status,
});

export const setAccessToken = (token: string): Action<string> => ({
  type: types.setAccessToken,
  payload: token,
});


export const setPages = (pages: Page[]): Action<Page[]> => ({
  type: types.setPages,
  payload: pages,
});

export const cancelRequestLogin = (): Action<void> => ({
  type: loginState.cancel
})

export const signOut = (): Action<void> => ({
  type: loginState.logout
})
