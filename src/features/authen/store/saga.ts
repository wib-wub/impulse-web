import axios from 'axios';
import firebase from 'firebase/app';
import type * as H from 'history';
import type { Task } from 'redux-saga';
import type { CallEffect, CancelEffect, CancelledEffect, ForkEffect, PutEffect, TakeEffect, AllEffect } from 'redux-saga/effects';
import { all, call, cancel, cancelled, fork, put, take } from 'redux-saga/effects';
import { facebook } from '../../../config';
import { firebaseAuth, firebaseFirestore } from '../../../services/firebase';
import { setAccessToken, setLoginState, setPages, setUser } from './actions';
import { loginState, Page, types, UserInfo } from './types';

function authorizeFB(): Promise<firebase.auth.UserCredential> {
  const fbProvider = new firebase.auth.FacebookAuthProvider();
  fbProvider.addScope('pages_read_engagement');
  fbProvider.addScope('pages_manage_metadata');
  fbProvider.addScope('email');
  fbProvider.addScope('public_profile');
  fbProvider.addScope('pages_show_list');
  fbProvider.addScope('pages_manage_posts');
  fbProvider.addScope('pages_read_user_content');
  return firebaseAuth.signInWithPopup(fbProvider);
}

function deauthorizeFB(): Promise<void> {
  return firebaseAuth.signOut();
}

function* handleLogout(task) {
  const action = yield take<Action<void>>([loginState.logout, loginState.failed]);
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  if (action.type === loginState.logout) {
    yield cancel(task as Task);
    yield put(setUser(null));
    yield put(setAccessToken(''));
    yield put(setPages([]));
    yield call(deauthorizeFB);
  }
}

function saveUserInfo(data: UserInfo): Promise<void> {
  return firebaseFirestore.collection('Users').doc(data.uid).set(data, { merge: true });
}

function getUserProfilebyUid(uid: string): Promise<UserInfo | Error | null> {
  return new Promise<UserInfo | Error | null>((resolve, reject) => {
    firebaseFirestore.collection('Users').doc(uid).get().then(docSnap => {
      if (docSnap.exists) {
        const data = docSnap.data();
        resolve({
          uid: data?.uid,
          email: data?.email,
          photoURL: data?.photoURL,
          accessToken: data?.accessToken,
        });
      } else {
        resolve(null);
      }
    }).catch((error) => {
      reject(error);
    });
  });
}

async function getPages(accessToken: string): Promise<Page[]> {
  // eslint-disable-next-line no-useless-catch
  try {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const { data: response } = await axios.get(`${facebook.url}/me/accounts?access_token=${accessToken}&fields=picture,name,access_token`);
    return response.data.map((page) => ({
      accessToken: page?.access_token,
      id: page?.id,
      name: page?.name,
      picture: page?.picture?.data?.url,
    }));
  } catch (e) {
    console.error(e);
    return [];
  }
}

export function* authorize(history: H.History): Generator<PutEffect | CallEffect | CancelledEffect> {
  try {
    yield put(setLoginState(loginState.logging));
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const { credential: { accessToken }, user } = yield call(authorizeFB);
    yield call(saveUserInfo, {
      uid: user.uid,
      email: user.email,
      photoURL: user.photoURL,
      accessToken: accessToken,
    });
    yield put(setUser({
      uid: user.uid,
      email: user.email,
      photoURL: user.photoURL,
    }));
    yield put(setAccessToken(accessToken));
    const pages = yield call(getPages, accessToken);
    yield put(setPages(pages as Page[]));
    yield put(setLoginState(loginState.logged));
    history.push('/');
  } catch (e) {
    console.error(e);
    yield put(setLoginState(loginState.failed));
  } finally {
    if (yield cancelled()) {
      yield put(setLoginState(loginState.cancel));
      history.push('/');
    }
  }
}

export function* getUserProfile(payload: string): Generator<PutEffect | CallEffect | CancelledEffect> {
  try {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const { accessToken, ...userInfo } = yield call(getUserProfilebyUid, payload);
    yield put(setUser({
      ...userInfo,
    }));
    yield put(setAccessToken(accessToken));
    const pages = yield call(getPages, accessToken);
    yield put(setPages(pages as Page[]));
  } catch (e) {
    console.error(e);
    yield put(setLoginState(loginState.failed));
  } finally {
    if (yield cancelled()) {
      yield put(setLoginState(loginState.cancel));
    }
  }
}

type LoginFlow = Generator<TakeEffect | ForkEffect | PutEffect | CallEffect | CancelEffect>
type ExistLogin = Generator<TakeEffect | ForkEffect | CancelEffect | PutEffect | CallEffect>

export function* loginFlow(): LoginFlow {
  while (true) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const { payload } = yield take<Action<H.History>>(types.loginRequest);
    const task = yield fork(authorize, payload as H.History);
    yield *handleLogout(task);
  }
}

export function* existLogin(): ExistLogin {
  while (true) {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const { payload } = yield take<Action<string>>(types.getUserInfo);
    const task = yield fork(getUserProfile, payload);
    yield *handleLogout(task);
  }
}

export function* authSaga(): Generator<AllEffect<ExistLogin | LoginFlow>> {
  yield all([
    loginFlow(),
    existLogin(),
  ]);
}
