export interface UserInfo {
  uid: string;
  email?: string | null;
  photoURL?: string | null;
  accessToken?: string | null;
}

export interface AuthReducer {
  userInfo: UserInfo | null;
  loginStatus: loginState | null;
  accessToken: string;
  pages: Page[]
}

export interface Page {
  accessToken: string;
  id: string;
  name: string;
  picture?: string;
}

export enum types {
  setUser = 'SET_USER',
  setLoginState = 'SET_LOGIN_STATE',
  loginRequest = 'LOGIN_REQUEST',
  setAccessToken = 'SET_FB_ACCESS_TOKEN',
  getUserInfo = 'USER_INFO_REQUEST',
  setPages = 'SET_PAGE'
}

export enum loginState {
  logging = 'LOGGING',
  logged = 'LOGGED',
  failed = 'FAILED',
  cancel = 'CANCEL',
  logout = 'LOGOUT'
}
