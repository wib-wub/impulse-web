import type { AuthReducer, Page, UserInfo } from './types';
import { loginState, types } from './types';

const initState: AuthReducer = {
  userInfo: null,
  accessToken: '',
  pages: [],
  loginStatus: null
};

export default (state = initState, action: Action<UserInfo | loginState | null | string | Page[]>): AuthReducer => {
  switch (action.type) {
    case types.setUser:
      return <AuthReducer>{ ...state, userInfo: action.payload };
    case types.setLoginState:
      return <AuthReducer>{ ...state, loginStatus: action.payload };
    case types.setAccessToken:
      return <AuthReducer>{ ...state, accessToken: action.payload };
    case types.setPages:
      return <AuthReducer>{ ...state, pages: action.payload };
    default : {
      return state;
    }
  }
}
