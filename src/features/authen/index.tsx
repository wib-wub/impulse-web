import React, { ReactElement, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import LoginLayout from '../../layouts/login.layout';
import { firebaseAuth } from '../../services/firebase';
import type { Reducers } from '../../store';
import { loginState, types } from './store';


function Authentication(): ReactElement {
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();
  const { userInfo } = useSelector((store: Reducers) => store.auth);

  const handleLogin = (): void => {
    dispatch({ type: types.loginRequest, payload: history });
  };

  const handleLogout = (): void => {
    dispatch({ type: loginState.logout });
  };

  useEffect(() => {
    firebaseAuth.onAuthStateChanged((user) => {
      if (user) {
        dispatch({ type: types.getUserInfo, payload: user.uid });
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        history.push(location.state.from);

      }
    });


  }, [firebaseAuth]);


  return (
    <LoginLayout>
      <div className="container">
        <h1>Login</h1>
        {
          userInfo ? <button className="btn btn-secondary" onClick={handleLogout}>logout</button> :
            <button className="btn btn-primary" onClick={handleLogin}>Continue with Facebook</button>
        }
      </div>
    </LoginLayout>
  );
}

export default Authentication;
