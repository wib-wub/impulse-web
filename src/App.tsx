import 'bootstrap/dist/css/bootstrap.css';
import React, { ReactElement } from 'react';
import Routes from './routes';
import Redux from './store';

function App(): ReactElement {
  return (
    <Redux>
      <Routes/>
    </Redux>
  );
}

export default App;
