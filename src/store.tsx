import React from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga';
// import { take } from 'redux-saga/effects';

import type { AuthReducer } from './features/authen/store';
import { authenReducer, authSaga } from './features/authen/store';
import type { HomeReducer } from './features/home/store';
import { homeReducer, homeSaga } from './features/home/store';

export interface Reducers {
  auth: AuthReducer;
  home: HomeReducer;
}

const sagaMiddleware = createSagaMiddleware();
const composeArgs = [
  applyMiddleware(sagaMiddleware),
];

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
if (window && window.__REDUX_DEVTOOLS_EXTENSION__) {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
  composeArgs.push(window.__REDUX_DEVTOOLS_EXTENSION__());
}

// function* logActions() {
//   while (true) {
//     const action = yield take() // correct
//     console.log(action)
//   }
// }

const reducers = {
  auth: authenReducer,
  home: homeReducer,
};
const rootReducer = combineReducers(reducers);
const store = createStore(rootReducer, compose(...composeArgs));

sagaMiddleware.run(authSaga);
sagaMiddleware.run(homeSaga);
// sagaMiddleware.run(logActions)

interface Props {
  children: React.ReactElement
}

function Store(props: Props): React.ReactElement {
  return (
    <Provider store={store}>
      {props.children}
    </Provider>
  );
}

export default Store;
